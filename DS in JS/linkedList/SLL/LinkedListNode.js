module.exports =  function LinkedListNode(value, next = null) {
    this.value = value;
    this.next = next;

  this.toString = function(callback) {
    return callback ? callback(this.value) : `${this.value}`;
  }
}
