//var LinkedListNode = require('./LinkedListNode');
//import reverseTraversal from "../../../javascript-algorithms/src/algorithms/linked-list/reverse-traversal/reverseTraversal";

var Comparator  = require('./Comparator');


function LinkedListNode(value, next = null) {
  this.value = value;
  this.next = next;
  this.compare = new Comparator();
  this.toString = function(callback) {
    return callback ? callback(this.value) : `${this.value}`;
  }
}

//export default  LinkedList
function LinkedList() {
  /**
   * @param {Function} [comparatorFunction]
   */
    /** @var LinkedListNode */
    this.head = null;

    /** @var LinkedListNode */
    this.tail = null;

  /**
   * @param {*} value
   * @return {LinkedList}
   */
  this.prepend = function(value) {
    // Make new node to be a head.
    const newNode = new LinkedListNode(value, this.head);
    this.head = newNode;

    // If there is no tail yet let's make new node a tail.
    if (!this.tail) {
      this.tail = newNode;
    }

    return this;
  }

  /**
   * @param {*} value
   * @return {LinkedList}
   */
  this.append = function(value) {
    const newNode = new LinkedListNode(value);

    // If there is no head yet let's make new node a head.
    if (!this.head) {
      this.head = newNode;
      this.tail = newNode;

      return this;
    }

    // Attach new node to the end of linked list.
    this.tail.next = newNode;
    this.tail = newNode;

    return this;
  }

  this.equal = function(a, b) {
    if (a === b) {
      return 1;
    }
  }

  /**
   * @param {*} value
   * @return {LinkedListNode}
   */
  this.delete = function (value) {
    if (!this.head) {
      return null;
    }

    let deletedNode = null;

    // If the head must be deleted then make next node that is differ
    // from the head to be a new head.
    while (this.head && this.equal(this.head.value, value)) {
      deletedNode = this.head;
      this.head = this.head.next;
    }

    let currentNode = this.head;

    if (currentNode !== null) {
      // If next node must be deleted then make next node to be a next next one.
      while (currentNode.next) {
        if (this.equal(currentNode.next.value, value)) {
          deletedNode = currentNode.next;
          currentNode.next = currentNode.next.next;
        } else {
          currentNode = currentNode.next;
        }
      }
    }

    // Check if tail must be deleted.
    if (this.equal(this.tail.value, value)) {
      this.tail = currentNode;
    }

    return deletedNode;
  }

  /**
   * @param {Object} findParams
   * @param {*} findParams.value
   * @param {function} [findParams.callback]
   * @return {LinkedListNode}
   */
  this.find = function({ value = undefined, callback = undefined }) {
    if (!this.head) {
      return null;
    }
    console.log("val-->",value)
    let currentNode = this.head;
    let indexToNode = 0

    while (currentNode) {
      // If callback is specified then try to find node by callback.
      if (callback && callback(currentNode.value)) {
        //return currentNode;
        return  indexToNode;
      }

      console.log("\n\n this.equal(currentNode.value, value-->",this.equal(currentNode.value, value),currentNode.value,value)
      // If value is specified then try to compare by value..
      if (value !== undefined && this.equal(currentNode.value, value)) {
        return  indexToNode;

//        return currentNode;
      }
      indexToNode+= 1;
      currentNode = currentNode.next;
    }

    return null;
  }

  /**
   * @return {LinkedListNode}
   */
  this.deleteTail = function() {
    const deletedTail = this.tail;

    if (this.head === this.tail) {
      // There is only one node in linked list.
      this.head = null;
      this.tail = null;

      return deletedTail;
    }

    // If there are many nodes in linked list...

    // Rewind to the last node and delete "next" link for the node before the last one.
    let currentNode = this.head;
    while (currentNode.next) {
      if (!currentNode.next.next) {
        currentNode.next = null;
      } else {
        currentNode = currentNode.next;
      }
    }

    this.tail = currentNode;

    return deletedTail;
  }

  /**
   * @return {LinkedListNode}
   */
  this.deleteHead = function() {
    if (!this.head) {
      return null;
    }

    const deletedHead = this.head;

    if (this.head.next) {
      this.head = this.head.next;
    } else {
      this.head = null;
      this.tail = null;
    }

    return deletedHead;
  }

  /**
   * @param {*[]} values - Array of values that need to be converted to linked list.
   * @return {LinkedList}
   */
  this.fromArray = function(values) {
    values.forEach(value => this.append(value));

    return this;
  }

  /**
   * @return {LinkedListNode[]}
   */
  this.toArray = function() {
    const nodes = [];

    let currentNode = this.head;
    while (currentNode) {
      nodes.push(currentNode);
      currentNode = currentNode.next;
    }

    return nodes;
  }

  this.showList = function(){
    let currentNode = this.head;
    let list = ''
    while (currentNode) {
      list += ` ${currentNode.toString()}`
      currentNode = currentNode.next;
    }
    return list;

  }

  /**
   * @param {function} [callback]
   * @return {string}
   */
  this.toString = function(callback) {
    return this.toArray().map(node => node.toString(callback)).toString();
  }

  /**
   * Reverse a linked list.
   * @returns {LinkedList}
   */
  this.reverse = function() {
    let currNode = this.head;
    let prevNode = null;
    let nextNode = null;

    while (currNode) {
      // Store next node.
      nextNode = currNode.next;

      // Change next node of the current node so it would link to previous node.
      currNode.next = prevNode;

      // Move prevNode and currNode nodes one step forward.
      prevNode = currNode;
      currNode = nextNode;
    }

    // Reset head and tail.
    this.tail = this.head;
    this.head = prevNode;

    return this;
  }

  /*
  swap two adjacent node in a linked list.
   e.g. 1)   Input a->b->c->d->e->f, Output : b->a->d->c->f->e
        2) Input a->b->c->d->e, Output : b->a->d->c->e
  * */
  this.swapAdjacent = function(){
    var recurse = function(current) {
      if (!current)
        return this;
      if (current.next) {
        var save = current.next.value;
        current.next.value = current.value;
        current.value = save;
        current = current.next;
      }
      return recurse(current.next);
    }.bind(this);
    return recurse(this.head);
  }

  /*
  swap two adjacent node in a linked list.
   e.g. 1)   Input a->b->c->d->e->f, Output : a->f->b->e->c->d

   Steps:
   1. reverse the second half list
   2. now u have first list in one pointer(first) and second list in sec pointer(sec)
   3. Start interleving the elements in fist and sec list

  * */
  this.swapKwithNminusK = function(){
  //Another approach: have 3 pointers, fast , slow and remember pointers, wen fast at end, slow in mid, point remember to slow,
    //move the slow till end n push to stack, now point the slow to head, print slow and pop one from stack and print till the
    //stack is empty OR the slow == remember


    //Normal reverse logic
    var reverse = function(currNode) {
      let nextNode = null, prevNode = null;
      while (currNode) {
        // Store next node.
        nextNode = currNode.next;

        // Change next node of the current node so it would link to previous node.
        currNode.next = prevNode;

        // Move prevNode and currNode nodes one step forward.
        prevNode = currNode;
        currNode = nextNode;
      }
      return prevNode;
    }

    var cur = this.head; //slow
    var fast = this.head; //fast pointer
    var pre = null;
    while(fast && fast.next){
      pre = cur;
      cur = cur.next;
      fast = fast.next.next
    }

    if(fast && fast.next){
      cur = cur.next;
    }

    let firstList = this.head;
    let finalStr = ''
    console.log("\n\n pree values-->",firstList && firstList.value,cur&&cur.value)
    cur = reverse(cur);
    let pre2 = cur;

    console.log("\n\n pree values-->",firstList && firstList.value,cur&&cur.value,cur && cur.value)
    while ((firstList || cur)){
      finalStr += ` ${firstList && firstList.value} ${cur && cur.value}`;
      firstList && (firstList = firstList.next);
      cur && (cur = cur.next);
    }
    pre.next = pre2;
    console.log("\n\n finalStr-->",finalStr);
  }
}


var ll = new LinkedList();
ll.append(6);
ll.append(1);
ll.append(2);
ll.append(4);
ll.append(5);
ll.append(9);
ll.append(8);
console.log('ll-->',ll.showList())
ll.swapKwithNminusK()
console.log('ll aft swap-->',ll.showList());
/*
console.log('rev-->',ll.reverse().toString())
console.log('ll-->',ll.prepend(10).toArray(),'    val2-->',ll.find({value:2}));
*/
