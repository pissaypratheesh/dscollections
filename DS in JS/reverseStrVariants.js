function reverse(str){
    if(!str || str.length <2) return str;

    return str.split('').reverse().join('');
}


function reverseWords(str){
    return str.split(' ').reverse().join(' ');
}

function completeRev(str) {
    let arr = str.split(' ').reverse();
    arr.forEach((a,i)=>{
        arr[i] = reverse(a)
    })
    return arr.join(' ')
}

function reverseInPlace(str){
    return str.split(' ').reverse().join(' ').split('').reverse().join('');
}

console.log("--->",reverse('abcd'),reverseWords('i am fine'),completeRev('i am fine'),reverseInPlace('I am the good boy'));
