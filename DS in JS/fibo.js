//in o(n)
function fibonacci(n){
    var fibo = [0, 1];

    if (n <= 2) return 1;

    for (var i = 2; i <=n; i++ ){
        fibo[i] = fibo[i-1]+fibo[i-2];
    }

    return fibo[n];
}

//in o(2^n)
// Recursive
 function fibonacci2(n){
    if(n<=1)
        return n;
    else
        return fibonacci2(n-1) + fibonacci2 (n-2);
}


console.log("--->",fibonacci(12),fibonacci2(12));
// 144


console.log("--->",isPrime(137));