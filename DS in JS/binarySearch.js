
//Basic n easy
function BinarySearch(arr,t) {
    var l = 0, h = arr.length, m;
    while (l < h) {
        m = Math.floor((l+h)/2);
        if (t <= arr[m])
            h = m;
        else
            l = m+1;
    }

    if (arr[l] === t) return l;
    else return -1;
}
console.log(BinarySearch([1,2,3,4,5,6,7],6));


function binSearch(arr, value) {
    let start = 0,
        end = arr.length - 1,
        mid;
    //Combined with ~, it can do a boolean check if an item exists in a string value:
    // ~ is used to get the boolean check, actual calc is  ~N -> -(N+1)
    while (~arr.indexOf(value)) {
        mid = Math.ceil((start + end) / 2);
        if (value < arr[mid]) {
            end = mid - 1;
        } else if (value > arr[mid]) {
            start = mid + 1;
        } else {
            break;
        }
    }
    return mid || null;
}
const arr = [1, 7, 15, 21, 35, 44, 52, 100];
console.log(binSearch(arr, 44)); // 5
console.log(binSearch(arr, 23)); // null

//recursion
function array_binarySearch(narray, delement) {
    var mposition = Math.floor(narray.length / 2);

    if (narray[mposition] === delement){
        return mposition;
    }
    else if (narray.length === 1)
    {
        return null;
    }
    else if (narray[mposition] < delement) {
        var arr = narray.slice(mposition + 1);
        var res = array_binarySearch(arr, delement);
        if (res === null)
        {
            return null;
        }
        else {
            return mposition + 1 + res;
        }
    }
    else {
        var arr1 = narray.slice(0, mposition);
        return array_binarySearch(arr1, delement);
    }
}

var myArray = [1, 2, 3, 5, 6, 7, 10, 11, 14, 15, 17, 19, 20, 22, 23];
console.log(array_binarySearch(myArray, 6));