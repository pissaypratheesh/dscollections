//MUST READ FOR PERMS N COMBS:
//https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/sets/permutations
//https://www.mathsisfun.com/combinatorics/combinations-permutations.html


//Write a JavaScript function that generates all combinations of a string.
// COMBINATIONS BY BIT MANUPLICATONS
function substrings(str1) {
    var array1 = str1.split('');
/*
    for (var x = 0, y=1; x < str1.length; x++,y++)
    {
        array1[x]=str1.substring(x, y);
    }
*/
    var combi = [];
    var temp= "";
    var slent = Math.pow(2, array1.length);

    for (var i = 0; i < slent ; i++)
    {
        temp= "";
        for (var j=0;j<array1.length;j++) {
            if ((i & Math.pow(2,j))){
                temp += array1[j];
            }
        }
        if (temp !== "")
        {
            combi.push(temp);
        }
    }
    console.log(combi.join("\n"));
}


/**
 * @param {*[]} permutationOptions
 * @param {number} permutationLength
 * @return {*[]}
 */
function permutateWithRepetitions(permutationOptions, permutationLength = permutationOptions.length) {
    //base case
    if (permutationLength === 1) {
        var perms = permutationOptions.map(permutationOption => [permutationOption]);
        console.log("\n\nONLY ONCEEEEEEEEEEEEEE permOPtionsss n perms-->",permutationOptions,perms);
        //ONLY ONCEEEEEEEEEEEEEE permOPtionsss n perms--> [ 'a', 'b', 'c' ] ,  [ [ 'a' ], [ 'b' ], [ 'c' ] ]
        return perms;
    }

    // Init permutations array.
    const permutations = [];

    // Get smaller permutations.
    const smallerPermutations = permutateWithRepetitions(permutationOptions, permutationLength - 1);
    console.log("\n\nsmallerPermutations-->",smallerPermutations,'\n permOpt-->',permutationOptions,'\n permLen-->',permutationLength)
    //smallerPermutations--> [ [ 'a' ], [ 'b' ], [ 'c' ] ]
    //  permOpt--> [ 'a', 'b', 'c' ]
    //  permLen--> 2


    //Next Itr-->
    //smallerPermutations--> [ [ 'a', 'a' ],
    //   [ 'a', 'b' ],
    //   [ 'a', 'c' ],
    //   [ 'b', 'a' ],
    //   [ 'b', 'b' ],
    //   [ 'b', 'c' ],
    //   [ 'c', 'a' ],
    //   [ 'c', 'b' ],
    //   [ 'c', 'c' ] ]
    //  permOpt--> [ 'a', 'b', 'c' ]
    //  permLen--> 3  and so on


    // Go through all options and join it to the smaller permutations.
    permutationOptions.forEach((currentOption) => {
        // for each of [a,b,c] , do...append at start to smaller perms
        smallerPermutations.forEach((smallerPermutation) => {
            var aftCon = [currentOption].concat(smallerPermutation)
            console.log("\n\n aft concattttt-->",aftCon,'  \n smalllperPer n currOpti-->',smallerPermutation,currentOption);
            // aft concattttt--> [ 'a', 'a', 'a' ]
            //  smalllperPer n currOpti--> [ 'a', 'a' ] a

            // aft concattttt--> [ 'a', 'a', 'b' ]
            //  smalllperPer n currOpti--> [ 'a', 'b' ] a
            // and so on..
            permutations.push(aftCon);
        });
    });

    return permutations;
}


//////////////////////////////////////////////////////



/**
 * @param {*[]} permutationOptions
 * @return {*[]}
 */
function permutateWithoutRepetitions(permutationOptions) {
    if (permutationOptions.length === 1) {
        return [permutationOptions];
    }

    // Init permutations array.
    const permutations = [];

    // Get all permutations for permutationOptions excluding the first element.
    const smallerPermutations = permutateWithoutRepetitions(permutationOptions.slice(1));

    // Insert first option into every possible position of every smaller permutation.
    const firstOption = permutationOptions[0];

    for (let permIndex = 0; permIndex < smallerPermutations.length; permIndex += 1) {
        const smallerPermutation = smallerPermutations[permIndex];
        console.log("smallerPermutation-->",smallerPermutation,'\n firstOpion-->',firstOption)//,'  \n permutationOptions-->',permutationOptions);
        //smallerPermutation--> [ 'd' ]
        // smallerPermutation--> [ 'c', 'd' ]
        // smallerPermutation--> [ 'd', 'c' ]
        // smallerPermutation--> [ 'b', 'c', 'd' ]
        // smallerPermutation--> [ 'c', 'b', 'd' ]
        // smallerPermutation--> [ 'c', 'd', 'b' ]
        // smallerPermutation--> [ 'b', 'd', 'c' ]
        // smallerPermutation--> [ 'd', 'b', 'c' ]
        // smallerPermutation--> [ 'd', 'c', 'b' ]


        // Insert first option into every possible position of smallerPermutation.
        for (let positionIndex = 0; positionIndex <= smallerPermutation.length; positionIndex += 1) {
            const permutationPrefix = smallerPermutation.slice(0, positionIndex);
            const permutationSuffix = smallerPermutation.slice(positionIndex);
            permutations.push(permutationPrefix.concat([firstOption], permutationSuffix));
        }
    }

    return permutations;
}




/**
 * @param {*[]} comboOptions
 * @param {number} comboLength
 * @return {*[]}
 */
function combineWithRepetitions(comboOptions, comboLength) {
    // If the length of the combination is 1 then each element of the original array
    // is a combination itself.
    if (comboLength === 1) {
        return comboOptions.map(comboOption => [comboOption]);
    }

    // Init combinations array.
    const combos = [];

    // Remember characters one by one and concatenate them to combinations of smaller lengths.
    // We don't extract elements here because the repetitions are allowed.
    comboOptions.forEach((currentOption, optionIndex) => {
        // Generate combinations of smaller size.
        const smallerCombos = combineWithRepetitions(comboOptions.slice(optionIndex), comboLength - 1);
        console.log("\n\n each of combo options-->",currentOption,' of ',comboOptions,optionIndex,' do fetch these smaller combos-->',smallerCombos);

        // Concatenate currentOption with all combinations of smaller size.
        smallerCombos.forEach((smallerCombo) => {
            combos.push([currentOption].concat(smallerCombo));
        });
    });

    return combos;
}
/**
 * @param {*[]} comboOptions
 * @param {number} comboLength
 * @return {*[]}
 */
//ONLY DIFF BETWEEN COMBINEWthRep and COMBINEWithoutRep is +1 in recursion call
function combineWithoutRepetitions(comboOptions, comboLength) {
    // If the length of the combination is 1 then each element of the original array
    // is a combination itself.
    if (comboLength === 1) {
        return comboOptions.map(comboOption => [comboOption]);
    }

    // Init combinations array.
    const combos = [];

    // Extract characters one by one and concatenate them to combinations of smaller lengths.
    // We need to extract them because we don't want to have repetitions after concatenation.
    comboOptions.forEach((currentOption, optionIndex) => {
        // Generate combinations of smaller size.
        const smallerCombos = combineWithoutRepetitions(comboOptions.slice(optionIndex + 1), comboLength - 1);

        // Concatenate currentOption with all combinations of smaller size.
        smallerCombos.forEach((smallerCombo) => {
            combos.push([currentOption].concat(smallerCombo));
        });
    });

    return combos;
}

//var val =['r','r','r','r','u','u','u','u'];
var    val = ['r','r','u']

var k = permutateWithoutRepetitions(val);
console.log("--->",k.length,val,k);