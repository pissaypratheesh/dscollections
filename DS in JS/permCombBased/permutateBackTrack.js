// uniq obj takes care of unique chars
var uniq = {};
function permutation(string,startIndex,endIndex){
    if (startIndex === endIndex) {
        uniq[string.join('')] = 1;
        return;
    }
    for (var i = startIndex; i <= endIndex; i++) {
        swap(string, startIndex, i);
        permutation(string, startIndex + 1, endIndex);
//        swap(string, i, startIndex); // backtrack
        swap(string, startIndex, i);
    }
}
function swap2(a,b){
    [a,b] = [b,a]
}

function swap (alphabets, index1, index2) {
    var temp = alphabets[index1];
    alphabets[index1] = alphabets[index2];
    alphabets[index2] = temp;
    return alphabets;
}
//var st = ['r','r','r','r','u','u','u','u'];
var st = ['a','b','c']
permutation(st,0,st.length-1);
var kys= Object.keys(uniq);
console.log("\n\n unieqaaaaa--->",kys.length, kys.join(','));




//Other ways:
function getAllPermutations(string) {
    var results = [];

    if (string.length === 1) {
        results.push(string);
        return results;
    }

    for (var i = 0; i < string.length; i++) {
        var firstChar = string[i];
        var charsLeft = string.substring(0, i) + string.substring(i + 1);
        var innerPermutations = getAllPermutations(charsLeft);
        for (var j = 0; j < innerPermutations.length; j++) {
            results.push(firstChar + innerPermutations[j]);
        }
    }
    return results;
}