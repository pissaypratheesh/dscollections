/*
Count number of strings (made of R, G and B) using given combination
We need to make a string of size n.
 Each character of the string is either ‘R’, ‘B’ or ‘G’.
 In the final string there needs to be
  at least r number of ‘R’,
  at least b number of ‘B’ and
  at least g number of ‘G’ (such that r + g + b <= n). We need to find number of such strings possible.

    Examples:

Input : n = 4, r = 1,
    b = 1, g = 1.
Output: 36
No. of 'R' >= 1,
    No. of ‘G’ >= 1,
    No. of ‘B’ >= 1 and
(No. of ‘R’) + (No. of ‘B’) + (No. of ‘G’) = n
then following cases are possible:
    1. RBGR and its 12 permutation
2. RBGB and its 12 permutation
3. RBGG and its 12 permutation
Hence answer is 36.

// Samples:
  int n = 4, r = 2;
    int b = 0, g = 1;

Output:
22

*/
var count=function(c,str) {
    var result = 0, i = 0;
    for(i;i<str.length;i++)
        if(str[i]==c)
            result++;
    return result;
};

function allowed(str,r,g,b) {
    //str = str.join('');
    console.log("\n\n\nstr n rgb-->",str,r,g,b);
    if(count('r',str) >= r){
        if(count('g',str) >= g){
            if(count('b',str) >= b){
                return true;
            }
        }
    }
}


function permutateWithRepetitions(permutationOptions, permutationLength = permutationOptions.length,r,g,b) {
    //base case
    if (permutationLength === 1) {
        var perms = permutationOptions.map(permutationOption => [permutationOption]);
        //console.log("\n\nONLY ONCEEEEEEEEEEEEEE permOPtionsss n perms-->",permutationOptions,perms);
        //ONLY ONCEEEEEEEEEEEEEE permOPtionsss n perms--> [ 'a', 'b', 'c' ] ,  [ [ 'a' ], [ 'b' ], [ 'c' ] ]
        return perms;
    }

    // Init permutations array.
    const permutations = [];

    // Get smaller permutations.
    const smallerPermutations = permutateWithRepetitions(permutationOptions, permutationLength - 1,r,g,b);


    // Go through all options and join it to the smaller permutations.
    permutationOptions.forEach((currentOption) => {
        // for each of [a,b,c] , do...append at start to smaller perms
        smallerPermutations.forEach((smallerPermutation) => {
            var aftCon = [currentOption].concat(smallerPermutation)
            console.log("rgb-->",aftCon.join(''));
            permutations.push(aftCon);
/*
           // NOTE: BELOW DOES NOT WORK AS THE RETURN TYPE IS IMPORTANT!!!
            console.log("rgb-->",aftCon.join(''),r,g,b)
            if(1){//allowed(aftCon.join(''),r,g,b)){
                //console.log("\n\n aft concattttt-->",r,g,b,aftCon,'  \n smalllperPer n currOpti-->',smallerPermutation,currentOption);
                // aft concattttt--> [ 'a', 'a', 'a' ]
                //  smalllperPer n currOpti--> [ 'a', 'a' ] a

                // aft concattttt--> [ 'a', 'a', 'b' ]
                //  smalllperPer n currOpti--> [ 'a', 'b' ] a
                // and so on..
            }
*/
        });
    });
    console.log("\n\n permulationnnn-->",permutations);
    return permutations;
}

var val = ['r','g','b'];
var k = permutateWithRepetitions(val,4,1,1,1);
console.log("--->",k.length,val,k);

/*

Recommended: Please solve it on “PRACTICE ” first, before moving on to the solution.
    As R, B and G have to be included atleast for given no. of times. Remaining values = n -(r + b + g).
    Make all combinations for the remaining values.
    Consider each element one by one for the remaining values and sum up all the permuations.
    Return total no. of permutations of all the combinations.
    filter_none
edit
play_arrow

brightness_4
// C++ program to count number of possible strings
// with n characters.
#include<bits/stdc++.h>
using namespace std;

// Function to calculate number of strings
int possibleStrings( int n, int r, int b, int g)
{
    // Store factorial of numbers up to n
    // for further computation
    int fact[n+1];
    fact[0] = 1;
    for (int i = 1; i <= n; i++)
    fact[i] = fact[i-1] * i;

    // Find the remaining values to be added
    int left = n - (r+g+b);
    int sum = 0;

    // Make all possible combinations
    // of R, B and G for the remaining value
    for (int i = 0; i <= left; i++)
    {
        for (int j = 0; j<= left-i; j++)
        {
            int k = left - (i+j);

            // Compute permutation of each combination
            // one by one and add them.
            sum = sum + fact[n] /
                (fact[i+r]*fact[j+b]*fact[k+g]);
        }
    }

    // Return total no. of strings/permutation
    return sum;
}

// Drivers code
int main()
{
    int n = 4, r = 2;
    int b = 0, g = 1;
    cout << possibleStrings(n, r, b, g);
    return 0;
}


 */