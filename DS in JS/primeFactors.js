/*
step-1: Any number will not be divisible by a number bigger than half of it. for example, 13 will never be divisible by 7, 8, 9 .. it could be as big as half of it for even number. for example, 16 will be divisible by 8 but will never be by 9, 10, 11, 12...
Decision: a number will never be divisible by a number bigger than half of its values. So, we dont have to loop 50%
step-2: Now, if a number is not divisible by 3. (if it is divisible by 3, then it wouldn't be a prime number). then it would be divisible any number bigger than the 1/3 of its value. for example, 35 is not divisible by 3. hence it will be never divisible by any number bigger than 35/3 will never be divisible by 12, 13, 14 ... if you take an even number like 36 it will never be divisible by 13, 14, 15
Decision: a number could be divisible by numbers 1/3 of its value.
step-3: For example u have the number 127. 127 is not divisible by 2 hence you should check upto 63.5. Secondly, 127 is not divisible by 3. So, you will check up to 127/3 approximately 42. It is not divisible by 5, divisor should be less than 127/5 approximately 25 not by 7. So, where should we stop?
Decision: divisor would be less than Math.sqrt (n)
* */


/*
NOTE:
This property can be proved using counter statement. Let a and b be two factors of n such that a*b = n.
 If both are greater than √n, then a.b > √n, * √n, which contradicts the expression “a * b = n”.
* */
//Below prints only prime factors
function primeFact(n) {

    let facts = [];
    while (n%2 == 0)
    {
        facts.push(2);
        n = n/2;
    }

    // n must be odd at this point.  So we can skip
    // one element (Note i = i +2), cz +1 would be even number
    for (let i = 3; i <= Math.sqrt(n); i = i+2)
    {
        console.log('\n\n\n i and n-->',i,n)
        // While i divides n, print i and divide n
        while (n%i == 0)
        {
            console.log("\n\n in while-->",i,n)
            facts.push(i);
            n = n/i;
        }
    }

    // This condition is to handle the case when n
    // is a prime number greater than 2
    if (n > 2)
        facts.push( n);

    return facts;
}

//From w3resource... all the factors
//Below prints ALL the  factors

function factors(n) {
    var num_factors = [], i;

    for (i = 1; i <= Math.floor(Math.sqrt(n)); i += 1){
        if (n % i === 0) {
            console.log("\n\n pushing i-->",i)
            num_factors.push(i);
            if (n / i !== i){
                console.log("\n\n pushing n/i-->",n/i,n,i);
                num_factors.push(n / i);
            }
        }
    }
    num_factors.sort(function(x, y) {
        return x - y;
    });  // numeric sort
    return num_factors;
}

//ES6
const factorsOf = (n) => [...Array(Math.abs(n) + 1).keys()]
    .slice(1)
    .filter(el => n % el == 0);

/*
console.log(factors(15));  // [1,3,5,15]
console.log(factors(16));  // [1,2,4,8,16]
console.log(factors(17));  // [1,17]
*/

console.log("--->",primeFact(315),factors(315),factorsOf(315));