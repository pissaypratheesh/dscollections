function makeComparer(order) {
    var ap = Array.prototype;

    // mapping from character -> precedence
    var orderMap = {},
        max = order.length + 2;
    ap.forEach.call(order, function(char, idx) {
        orderMap[char] = idx + 1;
        //console.log("\n\n char n idx-->",char,idx,orderMap);
    });

    function compareChars(l, r) {
        var lOrder = orderMap[l] || max,
            rOrder = orderMap[r] || max;
        console.log("\n in compareChars-->",l,r);
        return lOrder - rOrder;
    }

    function compareStrings(l, r) {
        var minLength = Math.min(l.length, r.length);
        var result = ap.reduce.call(l.substring(0, minLength), function (prev, _, i) {
            console.log("prev _ i-->",prev,_,i);
            return prev || compareChars(l[i], r[i]);
        }, 0);
        console.log(" l r -->",l.substring(0, minLength),r , result);

        return result || (l.length - r.length);
    }

    return compareStrings;
}

var wordArray = ['apple', 'abbot', 'aatrophy', 'banana',
    'berry',  'cherrypie','cherry', 'candy',
    'grapefruit', 'pear', 'pizza', 'zebra',
    'cigarette', 'guitar'];
/*
var comparer = makeComparer('abcdefghijklmnopqrstuvwxyz');
console.log(wordArray.slice().sort(comparer));
*/

var weirdComparer = makeComparer("kwfhjrsbdtqmxaopzvieulgcny");
console.log(wordArray,"\nNew comparator-->",wordArray.slice().sort(weirdComparer));

/*

Was asked to me in FB and this is wat went on:
    words = ["cca", "cbc", "cbb"]
alphabet = ['c', 'a', 'b']
output: true

"ab" < "abc"
"abc"> "ab"

"cca" < "cbc"
"cbc"  "cca"

"abb", "abc"

'aba' 'abc'
function checkOrder(alphabet,str1,str2){
    let minLen = Math.min(str1.length,str2.length);
    let samePattern = 0;
    for(let f=0;f<minLen;f++){
        if(str1[f] === str2[f]){
            samePattern++;
            continue;
        }
        if(alphabet.indexOf(str2[f]) > alphabet.indexOf(str1[f]){
            continue;
        }
        return false;
    }
    if(samePattern === minLen){
        if(str1.length > str2.length){
            return false;
        }
    }
    return true;
}


function isValidList(words,alphabet){
    for(let i = 0;i <= words.length-2;i++){
        if(!checkOrder(alphabet,words[i].charAt(0),words[i+1].charAt(0))){
            return false;
        }
    }



    for(let i = 0;i <= words.length-2;i++){
        if(!checkOrder(alphabet,words[i],words[i+1])){
            return false;
        }
    }
}
return true;
}
*/
