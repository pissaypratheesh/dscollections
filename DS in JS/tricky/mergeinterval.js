/*
You have two sorted arrays, where each element is an interval. Now, merge the two array, overlapping intervals can be merged as a single one.

    I/P :
List 1 [1,2] , [3,9]
List 2 [4,5], [8, 10], [11,12]

O/P [1,2], [3,10], [11,12]
*/

//NOT EASY TO UNDERSTAND.. SO IGNORE  N CHECK THE SECOND SOLUTION
function  merge2(x,y) {
    var res = [];
    var i = 0;
    var j = 0;
    while (i < x.length && j < y.length) {
        if (x[i,1] < y[j,0]) {
            res.push(x[i]);
            i++;
            continue;
        }
        if (y[i,1] < x[j,0]) {
            res.push(y[i]);
            j++;
            continue;
        }
        y[j,0] = Math.min(x[i,0], y[j,0]);
        y[j,1] = Math.max(x[i,1], y[j,1]);
        i++;
    }
    return res;
}



///////////
var numbers = [
    [10, 20],
        [30, 40],
        [40, 50],
        [45, 70],
]
var numbers2= [
    [10, 20],
        [19, 40],
        [40, 60],
        [70, 80],
]

function merge(ranges) {
    var result = [], last;
    console.log("\n b4 sort-->",ranges);
    ranges.sort(function(a, b) { return a[0]-b[0] || a[1]-b[1] });
    console.log("\n Afr sort-->",ranges);

    ranges.forEach(function (r) {
        // Hack to remember... always compare NEW[LOW_BOUND] AND NEW[UP_BOUND]  to LAST[UP_BOUND]
        //   AND ............. UPDATE COMPLETE LAST INTERVAL IF INTERVAL IS NOT OVERLAPPING

        //Always compare with the UPBOUND of the LAST Updated value

        //New intervals --> push complete obj and update complete last...New[low_bound] > last[up_bound]
        //Case of NEW UPDATE interval found
        if (!last || r[0] > last[1])
            result.push(last = r);

        //OVERLAPPING CASE 1. Update last[up_bound], if NEW interval's New[up_bound] > last[up_bound]
        //Case of Extenstion of the last update val
        else if (r[1] > last[1])
            last[1] = r[1];
        //OVERLAPPING CASE 2. IGNORE!!
    });

    return result;
}
let r = [ [70, 80],[10, 20], [19, 40], [40, 60]];
console.log('--->',JSON.stringify(merge(r)));