/*
Given a list of arrays of time intervals, write a function that calculates the total amount of time covered by the intervals.
    For example:
    input = [(1,4), (2,3)]
return 3
input = [(4,6), (1,2)]
return 3
input = {{1,4}, {6,8}, {2,4}, {7,9}, {10, 15}}
return 11
*/


function intervalTotalRange(intervals) {
    var starts = intervals.map(i => i.start).sort((a, b) => a - b);
    var ends = intervals.map(i => i.end).sort((a, b) => a - b);
    var totalRange = 0;
    var currentStart = 0;
    var currentCount = 0;
    console.log("\n\n starts n ends-->",starts,ends);
    for (var i = 0, j = 0; i < starts.length && j < ends.length;) {
        console.log("\n i,j-->",i,j,starts[i],ends[j],starts[i] <= ends[j]);
        //checks for overlapping.

        //below is not overlapping
        if (starts[i] <= ends[j]) {
            // start point
            if (currentCount == 0) {
                currentStart = starts[i];
            }
            console.log("\nstart point ++currentCount and  ++i->",currentCount,i)
            currentCount++;
            i++;
        } else {
            //new interval start
            console.log("\nEND point --currentCount and  i->",currentCount,currentStart,ends[j] ,i,j)
            // finish point
            currentCount--;
            if (currentCount == 0) {
                totalRange += (ends[j] - currentStart);
            }
            j++;
        }
    }
    totalRange += (ends[ends.length - 1] - currentStart);
    return totalRange;
}

var ints = [
    {
        start:1,
        end:4
    },
    {
        start:6,
        end:8
    },
    {
        start:2,
        end:4
    },
    {
        start:7,
        end:9
    },
    {
        start:10,
        end:15
    },
]
//console.log("\nintervalTotalRange-->",intervalTotalRange(ints))



function merge(ranges) {
    var result = [], last, totalIntervalCount = 0;
    console.log("\n b4 sort-->",ranges);
    ranges.sort(function(a, b) { return a[0]-b[0] || a[1]-b[1] });
    console.log("\n Afr sort-->",ranges);

    ranges.forEach(function (r) {
        // Hack to remember... always compare NEW[LOW_BOUND] AND NEW[UP_BOUND]  to LAST[UP_BOUND]
        //   AND ............. UPDATE COMPLETE LAST INTERVAL IF INTERVAL IS NOT OVERLAPPING


        //New intervals --> push complete obj and update complete last...New[low_bound] > last[up_bound]
        if (!last || r[0] > last[1]){
            let v =  last = r;
            result.push(v);
            console.log("\n\n last n r-->",last,r, ' result-->',result);
            //NOTE: NEVER DO THIS BELOW....CZ last[1] will be updated which will update result[0][1], for verification try below code, the result will be unexpected
            //let v =  last = r;
            //result.push([...v]);

            //Note: This doesn’t safely copy multi-dimensional arrays. Array/object values are copied by reference instead of by value.
            //totalIntervalCount+= (last[1]-last[0])
            //REASON..SEE THE BELOW EXPLANATION, Cz ARRAY IN ARRAY IS COPU BY REFERENCE

        }

        //OVERLAPPING CASE 1. Update last[up_bound], if NEW interval's New[up_bound] > last[up_bound]
        else if (r[1] > last[1]){
            last[1] = r[1];
            console.log("\n\n Test result-->" ,result,last[1],last);
        }
        //OVERLAPPING CASE 2 and others. IGNORE!!
    });
    result.forEach((a)=>{
        totalIntervalCount+= (a[1]-a[0])
    })
    console.log("\n\ntotalIntervalCount-->",totalIntervalCount);
    return result;
}

let r = [ [70, 80],[10, 20], [19, 40], [40, 60]];
console.log('--->',JSON.stringify(merge(r)));

/*
Note: This doesn’t safely copy multi-dimensional arrays. Array/object values are copied by reference instead of by value.
imagine this approach is the least popular, given how trendy functional programming’s become in our circles.

    Pure or impure, declarative or imperative, it gets the job done!

    numbers = [1, 2, 3];
numbersCopy = [];

for (i = 0; i < numbers.length; i++) {
    numbersCopy[i] = numbers[i];
}
Note: This doesn’t safely copy multi-dimensional arrays. Since you’re using the = operator, it’ll assign objects/arrays by reference instead of by value.

    This is fine

numbersCopy.push(4);
console.log(numbers, numbersCopy);
// [1, 2, 3] and [1, 2, 3, 4]
// numbers is left alone
This is not fine

nestedNumbers = [[1], [2]];
numbersCopy = [];

for (i = 0; i < nestedNumbers.length; i++) {
    numbersCopy[i] = nestedNumbers[i];
}

numbersCopy[0].push(300);
console.log(nestedNumbers, numbersCopy);
// [[1, 300], [2]]
// [[1, 300], [2]]
// They've both been changed because they share references
*/
