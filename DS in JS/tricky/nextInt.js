
/*
given an array representing a non-negative integer (ex: 123 represented as [1,2,3]), return the next integer (output: [1,2,4]).
run through all edge cases (ex: [9,9,9,9,9,9,9,9] etc)

*/

var NextInteger = function (arr) {
    var carry = 0;
    var result = [];
    for (var i = arr.length - 1; i >= 0; i--) {
        var sum = 0
        var addNum = 0;
        if (i == arr.length - 1) {
            addNum = arr[i] + 1;
        } else {
            addNum = arr[i];
        }
        sum = carry + addNum;
        if (sum > 9) {
            carry = 1; sum = 0;
        }
        else {
            carry = 0;
        }
        result.unshift(sum);
    }

    if (carry > 0)
        result.unshift(carry);

}

function printSums(n, c1, c2, c3) {

    let sums = {0:1};
    let arr = []

    for(let sum = 1; sum <= n; sum++) {

        if(sums[sum - c1] || sums[sum - c2] || sums[sum - c3]) {
            arr.push(sum);
            console.log('\n -->',arr,sum,n)
            sums[sum] = 1;
        }
    }
    return arr;
}

console.log("\n\n -->",printSums(1000,10,15,55))