function findTriplet(arr,sum) {
    if(arr && arr.length){
        arr.sort((a,b)=>a-b);
        let subArrPairs = {};
        for(let i = 0;i<arr.length - 2;i++){
            let curSum = sum - arr[i];
            for(let j = i+1;j<arr.length;j++){
                if(subArrPairs[curSum - arr[j]]){
                    console.log("triplet -->",arr[i],arr[j],curSum-arr[j]);
                    return true;
                }
                subArrPairs[arr[j]] = 1;
            }

        }
        return;
    }
}

function findClosestTriplet(arr,sum) {
    if(arr && arr.length){
        arr.sort((a,b)=>a-b);
        let closestSoFar = Infinity;
        let closesSum = [];
        for(let i = 0;i<arr.length - 2;i++){
            let low = i+1, high = arr.length-1;
            // While there could be more pairs to check
            while (low < high) {

                // Calculate the sum of the current triplet
                let assSum = arr[i] +arr[low] +arr[high];

                // If the sum is more closer than
                // the current closest sum
                if (Math.abs(sum - assSum) < Math.abs(sum - closestSoFar)) {
                    closestSoFar = assSum;
                    closesSum = [arr[i] ,arr[low] ,arr[high]]

                }

                // If sum is greater then x then decrement
                // the second pointer to get a smaller sum
                if (assSum > sum) {
                    high-=1;
                }

                    // Else increment the first pointer
                // to get a larger sum
                else {
                    low+=1;
                }
            }



        }
        return {closestSoFar,closesSum};
    }
}
var arr = [ 1, 4, 45, 6, 10, 8 ]
var sum = 23;
var trip = 22;
console.log("findClosestTriplet --->", findClosestTriplet(arr,sum));
console.log("findTriplet --->", findTriplet(arr,trip));












