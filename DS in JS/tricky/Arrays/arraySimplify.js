/*
I/P [8, 3, 2, [5, 6, [9]], 6]

O/P 8+3+2+2*(5+6+3*(9))+6 => 95
*/
{{
    var input = [8,3,2,[5,6,[9]],6];

    var mySum = function(inputX, level){
        var result = 0;
        var temp;
        level = level + 1;
        for(var i = 0; i < inputX.length; i++){
            if(Array.isArray(inputX[i])){
                temp = mySum(inputX[i], level);
                result = result + (level * (temp));
            }else{
                result = result + inputX[i];
            }
        }
        return result;
    }

    var result = mySum(input, 1);
    console.log(result);
}}