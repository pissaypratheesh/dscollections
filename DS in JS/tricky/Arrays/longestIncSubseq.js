function findLongestIncreasingSequence(array) {
    var sequence = [],
        fork = null;

    // Always add the first value to the sequence
    sequence.push(array[0]);

    // Reduce the array with. Since no initial accumulator is given,
    // the first value in the array is used
    array.reduce(function (previous, current, index) {

        // If the current value is larger than the last, add it to the
        // sequence and return (i.e. check the next value)
        if(current > previous) {
            sequence.push(current);
            return current;
        }

        // If, however, the value is smaller, and we haven't had a fork
        // before, make one now, starting at the current value's index
        if(!fork && current < previous) {
            fork = findLongestIncreasingSequence(array.slice(index));
        }

        // Return the previous value if the current one is less or equal
        return previous;
    });

    // Compare the current sequence's length to the fork's (if any) and
    // return whichever one is larger
    return fork && fork.length > sequence.length ? fork : sequence;
}
/*
THese both approaches are wrong
//Another
function lis(a, r = [a[0]]){
    console.log('r -->',r,JSON.stringify(a));
    if(!a.length) return r;
    a.splice(0,1);
    r[r.length-1] < a[0] && r.push(a[0]);
    return lis(a,r);
}

function lis2(arr) {
    return arr.reduce((p,c,i) => {
        console.log("\n\n p c i-->",p,c,i)
        return i ?
        ((p[p.length-1] < c) ? p.concat(c) : p)
            : [c]
    } ,[]);
}
*/

var arr = ["87", "88", "91", "10", "22", "9",'1','2','3','4','5','6','7','8','9', "92", "94", "33", "21", "50", "41", "60", "80"];
//console.log(lis(arr));
//console.log('arr-->',JSON.stringify(arr))
console.log('arr-->',JSON.stringify(arr))
console.log('\n lis-->',findLongestIncreasingSequence(arr),JSON.stringify(arr)
);
// => [ 87, 88, 91, 92, 94 ])