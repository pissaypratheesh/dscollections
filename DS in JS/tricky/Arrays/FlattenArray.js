//// Flatten deeply nested array:
var arr = [1, 2, [3, 4, [5, 6]]];

//to enable deep level flatten use recursion with reduce and concat
function flatDeep(arr) {
    return arr.reduce((acc, val) => acc.concat(Array.isArray(val) ? flatDeep(val) : val), []);
}

flatDeep(arr);
// [1, 2, 3, 4, 5, 6]

// using .flat
var arr1 = [1, 2, [3, 4]];
arr1.flat();
// [1, 2, 3, 4]

var arr2 = [1, 2, [3, 4, [5, 6]]];
arr2.flat();
// [1, 2, 3, 4, [5, 6]]

var arr3 = [1, 2, [3, 4, [5, 6]]];
arr3.flat(2);
// [1, 2, 3, 4, 5, 6]

var arr4 = [1, 2, [3, 4, [5, 6, [7, 8, [9, 10]]]]];
arr4.flat(Infinity);
// [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

/*
MUST READDDDDDDDD
Flattening and array holes
The flat method removes empty slots in arrays:
*/

var arr5 = [1, 2, , 4, 5];
arr5.flat();
// [1, 2, 4, 5]

///

//Use Generator function
function* flatten(array) {
    for (const item of array) {
        if (Array.isArray(item)) {
            yield* flatten(item);
        } else {
            yield item;
        }
    }
}

var arr6 = [1, 2, [3, 4, [5, 6]]];
const flattened = [...flatten(arr6)];
// [1, 2, 3, 4, 5, 6]