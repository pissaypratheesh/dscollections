/*

k=2, l=[1,2,3,4,5,6]
output: l=[5,6,1,2,3,4]
In place O(1) space complexity

*/
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
function fisherYates(originalArray) {
    // Clone array from preventing original array from modification (for testing purpose).
    const array = originalArray.slice(0);

    for (let i = (array.length - 1); i > 0; i -= 1) {
        const randomIndex = Math.floor(Math.random() * (i + 1));
        [array[i], array[randomIndex]] = [array[randomIndex], array[i]];
    }

    return array;
}

var points = [40, 100, 1, 5, 25, 10];
//Very simple randomize function
points.sort(function(a, b){return 0.5 - Math.random()});

console.log("\n\n -->",fisherYates([1,2,3,4,5,6]))