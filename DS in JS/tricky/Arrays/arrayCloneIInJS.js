// MUST READ: https://www.freecodecamp.org/news/how-to-clone-an-array-in-javascript-1d3183468f6a/
/*
Note: This doesn’t safely copy multi-dimensional arrays. Array/object values are copied by reference instead of by value.

imagine this approach is the least popular, given how trendy functional programming’s become in our circles.

    Pure or impure, declarative or imperative, it gets the job done!

    numbers = [1, 2, 3];
numbersCopy = [];

for (i = 0; i < numbers.length; i++) {
    numbersCopy[i] = numbers[i];
}
Note: This doesn’t safely copy multi-dimensional arrays. Since you’re using the = operator, it’ll assign objects/arrays by reference instead of by value.

    This is fine

numbersCopy.push(4);
console.log(numbers, numbersCopy);
// [1, 2, 3] and [1, 2, 3, 4]
// numbers is left alone
This is not fine

nestedNumbers = [[1], [2]];
numbersCopy = [];

for (i = 0; i < nestedNumbers.length; i++) {
    numbersCopy[i] = nestedNumbers[i];
}

numbersCopy[0].push(300);
console.log(nestedNumbers, numbersCopy);
// [[1, 300], [2]]
// [[1, 300], [2]]
// They've both been changed because they share references
*/


Array.prototype.clone = function() {
    //return this.slice()
    return this.slice(0);
};

//In ES6
var numbers = [1, 2, 3];
var numbersCopy = [...numbers];

////
numbers = [1, 2, 3];
numbersCopy = [];

for (var i = 0; i < numbers.length; i++) {
    numbersCopy[i] = numbers[i];
}
///


/*
* NOTE:: SAFE DEEPCLONE
JSON.stringify turns an object into a string.

JSON.parse turns a string into an object.

Combining them can turn an object into a string, and then reverse the process to create a brand new data structure.

Note: This one safely copies deeply nested objects/arrays!

nestedNumbers = [[1], [2]];
numbersCopy = JSON.parse(JSON.stringify(nestedNumbers));

numbersCopy[0].push(300);
console.log(nestedNumbers, numbersCopy);

// [[1], [2]]
// [[1, 300], [2]]
// These two arrays are completely separate!

*
*
* */
