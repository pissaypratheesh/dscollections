/*

k=2, l=[1,2,3,4,5,6]
output: l=[5,6,1,2,3,4]
In place O(1) space complexity

*/
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var rotate = function(nums, k) {
    let len = nums.length
    let portion = nums.splice(len - (k%len), len)

    nums.unshift(...portion)
};