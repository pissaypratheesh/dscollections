/**
 * @param {number[]} candidates - candidate numbers we're picking from.
 * @param {number} remainingSum - remaining sum after adding candidates to currentCombination.
 * @param {number[][]} finalCombinations - resulting list of combinations.
 * @param {number[]} currentCombination - currently explored candidates.
 * @param {number} startFrom - index of the candidate to start further exploration from.
 * @return {number[][]}
 */
function combinationSumRecursive(
    candidates,
    remainingSum,
    finalCombinations = [],
    currentCombination = [],
    startFrom = 0
) {
    if (remainingSum < 0) {
        // By adding another candidate we've gone below zero.
        // This would mean that the last candidate was not acceptable.
        return finalCombinations;
    }

    if (remainingSum === 0) {
        // If after adding the previous candidate our remaining sum
        // became zero - we need to save the current combination since it is one
        // of the answers we're looking for.
        finalCombinations.push(currentCombination.slice());

        return finalCombinations;
    }

    // If we haven't reached zero yet let's continue to add all
    // possible candidates that are left.
    for (let candidateIndex = startFrom; candidateIndex < candidates.length; candidateIndex += 1) {
        const currentCandidate = candidates[candidateIndex];

        // Let's try to add another candidate.
        currentCombination.push(currentCandidate);

        console.log('\n currentCombination-->',candidateIndex,currentCombination,remainingSum - currentCandidate);
        // Explore further option with current candidate being added.
        combinationSumRecursive(
            candidates,
            remainingSum - currentCandidate,
            finalCombinations,
            currentCombination,
            candidateIndex
        );
       // console.log('\nBacktracking currentCombination-->',candidateIndex,currentCombination,remainingSum - currentCandidate);

        // BACKTRACKING.
        // Let's get back, exclude current candidate and try another ones later.
        currentCombination.pop();
    }

    return finalCombinations;
}

/**
 * Backtracking algorithm of finding all possible combination for specific sum.
 *
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
 function combinationSum(candidates, target) {
    return combinationSumRecursive(candidates, target);
}

console.log("\n\n-->",combinationSum([2, 3, 5], 8))



/*


/////////////////////ANother solution
var combinationSum = function(candidates, target) {
    var results = [];
    if (candidates === null || candidates.length === 0) {
        return results;
    }
    // sort it first!! O(NlogN)
    const sortedCandidates = candidates.sort((a, b) => {
        return a - b;
    });
    // O depends on how many answers are there, and the length of the answers?
    dfsHelper(0, [], sortedCandidates, target, results);
    return results;
};

function dfsHelper(startIndex, combinations, sortedCandidates, target, results) {
    // when to exit
    if (target === 0) {
        // donnot forget to deep copy please....
        results.push(combinations.slice());
        return;
    }
    for (let i = startIndex; i < sortedCandidates.length; i++) {
        // when to give up the remain candidates
        if (sortedCandidates[i] > target) {
            break;
        }
        combinations.push(sortedCandidates[i]);
        // it starts from i, not 0!!! 选了这个数下次还能选这个数，但不会再选它前面的数，不然结果有重复啊
        dfsHelper(i, combinations, sortedCandidates, target - sortedCandidates[i], results);
        combinations.pop();
    }
}
*/
