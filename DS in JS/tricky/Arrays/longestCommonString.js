//Reference: https://www.youtube.com/watch?v=BysNXJHzCEs&list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8&index=45
// and modified code from LongCommSeqeuence(https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/sets/longest-common-subsequence)
function longestCommonString(set1, set2) {
    // Init LCS matrix.
    const lcsMatrix = Array(set2.length + 1).fill(null).map(() => Array(set1.length + 1).fill(null));

    let skipList = Array.from(new Array(10), x => new Array(10).fill(0));//Array.from!

    console.log("\n\n\n lcsMatrix-->",lcsMatrix)

    // Fill first row with zeros.
    for (let columnIndex = 0; columnIndex <= set1.length; columnIndex += 1) {
        lcsMatrix[0][columnIndex] = 0;
    }

    // Fill first column with zeros.
    for (let rowIndex = 0; rowIndex <= set2.length; rowIndex += 1) {
        lcsMatrix[rowIndex][0] = 0;
    }
    var maxLocationIndices = [],
        maxVal = 0;
    // Fill rest of the column that correspond to each of two strings.
    for (let rowIndex = 1; rowIndex <= set2.length; rowIndex += 1) {
        for (let columnIndex = 1; columnIndex <= set1.length; columnIndex += 1) {
            if (set1[columnIndex - 1] === set2[rowIndex - 1]) {
                lcsMatrix[rowIndex][columnIndex] = lcsMatrix[rowIndex - 1][columnIndex - 1] + 1;
                if(lcsMatrix[rowIndex][columnIndex] > maxVal){
                    maxVal = lcsMatrix[rowIndex][columnIndex];
                    maxLocationIndices = [rowIndex,columnIndex];
                }
            }
            /* // Below is extra from LongestCommonSubsequence
            else {
                lcsMatrix[rowIndex][columnIndex] = Math.max(
                    lcsMatrix[rowIndex - 1][columnIndex],
                    lcsMatrix[rowIndex][columnIndex - 1],
                );
            }*/
        }
    }

    //Below code deviates from LCSequence

    // Calculate LCS based on LCS matrix.
    if (!maxVal) {
        // If the length of largest common string is zero then return empty string.
        return [''];
    }

    const longestComStr = [];
    let columnIndex = maxLocationIndices[1];
    let rowIndex = maxLocationIndices[0];
    console.log("\n\n\n matrix-->",lcsMatrix,maxVal,maxLocationIndices);
    while (columnIndex > 0 || rowIndex > 0) {
        if(lcsMatrix[rowIndex][columnIndex])
            longestComStr.unshift(set1[columnIndex - 1]);
        columnIndex-=1;
        rowIndex-=1;
    }

    return longestComStr;
}


console.log('--->',longestCommonString(
    ["a", "b", "c", "d", "a", "f"],
    ["z", "b", "c", "e", "f"],
))