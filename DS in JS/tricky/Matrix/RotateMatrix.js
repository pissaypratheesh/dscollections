// Must read: https://medium.com/front-end-weekly/matrix-rotation-%EF%B8%8F-6550397f16ab


/*
RULE TO REMEMBER:
    Reversing the matrix "before" the flip will yield a "clockwise" rotation.
    Reversing the matrix "after" the flip will yield a "counter-clockwise" rotation.
*/


const reverse = array => [...array].reverse();
const compose = (a, b) => x => a(b(x));

//FLip matrix on diagonal axis
const flipMatrix = matrix => (
    matrix[0].map((column, index) => (
        matrix.map(row => row[index])
    ))
);
/*
//Above FlipMatrix is same as below:
const matrix = [
  [1,2,3],
  [4,5,6],
  [7,8,9],
];
const indices = [0, 1, 2];
indices.map(index => (
  matrix.map(row => row[index])
));
*/

const rotateMatrix = compose(flipMatrix, reverse);
const rotateMatrixCounterClockwise = compose(reverse, flipMatrix);

const flipMatrixCounterClockwise = compose(reverse, rotateMatrix);

const matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
];

console.log("\n\n Given matrix-->",matrix);

console.log("\n\n rotate clockwise matrix-->",rotateMatrix(matrix));

console.log("\n\n rotate anticlockwise matrix-->",rotateMatrixCounterClockwise(matrix));

