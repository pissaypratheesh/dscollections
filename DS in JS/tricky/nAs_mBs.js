
/*
Given a length n, count the number of strings of length n that can be made using ‘a’, ‘b’ and ‘c’
 with at-most one ‘b’ and two ‘c’s allowed.
 */
//Ans link: https://www.careercup.com/question?id=6324318653382656
/*
#include <iostream>
using namespace std;

int helper(int n, int nb, int nc) {
if (n == 0)
return 1;
int result = helper(n-1, nb, nc);
if (nb > 0)
result += helper(n-1, nb-1, nc);
if (nc > 0)
result += helper(n-1, nb, nc-1);
return result;
}

int main () {
int n = 5;
int nb = 1;
int nc = 2;
cout << "Number of possible strings: " << helper(n, nb, nc) << "\n";
return 0;
}
* */


//From : https://www.geeksforgeeks.org/count-strings-can-formed-using-b-c-given-constraints/
/*
// C++ program to count number of strings
// of n characters with
#include<bits/stdc++.h>
using namespace std;

// n is total number of characters.
// bCount and cCount are counts of 'b'
// and 'c' respectively.
int countStr(int n, int bCount, int cCount)
{
    // Base cases
    if (bCount < 0 || cCount < 0) return 0;
    if (n == 0) return 1;
    if (bCount == 0 && cCount == 0) return 1;

    // Three cases, we choose, a or b or c
    // In all three cases n decreases by 1.
    int res = countStr(n-1, bCount, cCount);
    res += countStr(n-1, bCount-1, cCount);
    res += countStr(n-1, bCount, cCount-1);

    return res;
}

// Driver code
int main()
{
    int n = 3;  // Total number of characters
    cout << countStr(n, 1, 2);
    return 0;
}
* */


/* //https://www.careercup.com/question?id=6324318653382656
I think this can just be calculated (O(1)) (in code need to handle correctly cases of n < 3)
1+n+n+(n choose 2) + n*(n-1) + n*(n -1 choose 2)

1 - case of all As
n - case of all As and B
n - case of all As and C
(n choose 2) - case of all As and 2Cs
n*(n-1) - case of All As and B and C
n*(n -1 choose 2)- case of All As and B and 2 Cs
* */

// with at-most one ‘b’ and two ‘c’s allowed.
function nStrings (n) {
    var count = 0;
    var possiblities = ['a', 'b', 'c'];
//                       /  |  \
//                      a    b   c
//                     /|\  /|\   \ 
//                     abc  abc     a   
    var recurse = function (stringSoFar, bs, cs) {
        if(stringSoFar.length > n || bs > 1 || cs > 2) {
            return;
        } else if (stringSoFar.length === n) {
            count ++;
            return;
        } else {
            for(var i = 0; i < possiblities.length; i++) {
                var b = 0;
                var c = 0;
                if(possiblities[i] === 'b') {
                    b = 1;
                } else if (possiblities[i] === 'c') {
                    c = 1;
                }
                console.log("\nstringSoFar-->",stringSoFar,'  possiblities[i]-->',possiblities[i],i,bs,cs);
                recurse(stringSoFar + possiblities[i], bs + b, cs + c);
            }
        }

    };

    recurse('', 0, 0);

    return count;
}

console.log("\n-->n of 3",nStrings(3))
// time O(n^n)
// space O(n^n)

// ANother approach ....easy n reasonable
function tryout(n){
    //for n=>4
    var str = "";

    var aCount = 0;
    var bCount = 0;
    var cCount = 0;
    var answer = 0;

    for (let a = 0; a <= n; a++) {
        aCount = a;
        for (let b = 0; b <= n - aCount; b++)
        {
            bCount = b;
            if (bCount > 1)
                continue;
            cCount = n - (aCount + bCount);
            if (cCount > 2)
                continue;
            console.log('\n\n--->',aCount +" "+bCount+" "+cCount, '\n ans so far-->',answer);
            answer = answer + (factorial(n) / (factorial(aCount) * factorial(bCount) * factorial(cCount)));
        }
    }

    console.log('fin-->',answer);
}


function factorial(n) {
    var fin = 1;
    for(let i=1;i<=n;i++){
        fin=fin*i;
    }
    return fin;
}