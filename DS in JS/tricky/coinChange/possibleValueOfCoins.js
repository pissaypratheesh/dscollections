
/*
Given many coins of 3 different face values, print the combination sums of the coins up to 1000. Must be printed in order.

eg: coins(10, 15, 55)
print:
10
15
20
25
30
.
.
.
1000

*/


function printSums(n, c1, c2, c3) {

    let sums = {0:1};
    let arr = []

    for(let sum = 1; sum <= n; sum++) {

        if(sums[sum - c1] || sums[sum - c2] || sums[sum - c3]) {
            arr.push(sum);
            sums[sum] = 1;
            sum < 40 && console.log('\n -->',Object.keys(sums),sum,n)
            sum < 40 && console.log('sum sums[sum - c1] || sums[sum - c2] || sums[sum - c3] -->',sums[sum - c1],sum - c1 ,'||', sums[sum - c2] ,sum - c2,'||',sums[sum - c3],sum - c3)

        }
    }
    return arr;
}

console.log("\n\n -->",printSums(1000,10,15,55))