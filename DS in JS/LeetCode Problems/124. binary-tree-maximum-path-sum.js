// Given a binary tree, find the maximum path sum.

// For this problem, a path is defined as any sequence of nodes from some starting node to any node in the tree
// along the parent-child connections. The path must contain at least one node and does not need to go through the root.

// For example:
// Given the below binary tree,

//        1
//       / \
//      2   3
// Return 6.

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */


// THis sol is not that nice(Below JS);
    // REfer: https://www.geeksforgeeks.org/find-maximum-path-sum-in-a-binary-tree/
    //Logic:
/*
For each node there can be four ways that the max path goes through the node:
    1. Node only
2. Max path through Left Child + Node
3. Max path through Right Child + Node
4. Max path through Left Child + Node + Max path through Right Child

row

brightness_5
// C/C++ program to find maximum path sum in Binary Tree
#include<bits/stdc++.h>
using namespace std;

// A binary tree node
struct Node
{
    int data;
    struct Node* left, *right;
};

// A utility function to allocate a new node
struct Node* newNode(int data)
{
    struct Node* newNode = new Node;
    newNode->data = data;
    newNode->left = newNode->right = NULL;
    return (newNode);
}

// This function returns overall maximum path sum in 'res'
// And returns max path sum going through root.
int findMaxUtil(Node* root, int &res)
{
    //Base Case
    if (root == NULL)
        return 0;

    // l and r store maximum path sum going through left and
    // right child of root respectively
    int l = findMaxUtil(root->left,res);
    int r = findMaxUtil(root->right,res);

    // Max path for parent call of root. This path must
    // include at-most one child of root
    int max_single = max(max(l, r) + root->data, root->data);

    // Max Top represents the sum when the Node under
    // consideration is the root of the maxsum path and no
    // ancestors of root are there in max sum path
    int max_top = max(max_single, l + r + root->data);

    res = max(res, max_top); // Store the Maximum Result.

    return max_single;
}

// Returns maximum path sum in tree with given root
int findMaxSum(Node *root)
{
    // Initialize result
    int res = INT_MIN;

    // Compute and return result
    findMaxUtil(root, res);
    return res;
}

// Driver program
int main(void)
{
    struct Node *root = newNode(10);
    root->left        = newNode(2);
    root->right       = newNode(10);
    root->left->left  = newNode(20);
    root->left->right = newNode(1);
    root->right->right = newNode(-25);
    root->right->right->left   = newNode(3);
    root->right->right->right  = newNode(4);
    cout << "Max path sum is " << findMaxSum(root);
    return 0;
}
*/

var maxPathSum = function(root) {
    return dcHelper(root).any2any; // no need to check null, helper will check it
};

function dcHelper(root) {
    if (root === null) { // only check null, no need to check .left===null, right===null, the next level helper will check it!
        return {root2any: -Infinity,
                any2any: -Infinity
        }
    }
    let left = dcHelper(root.left);// no need to check .left===null, right===null, the next level helper will check it!
    let right = dcHelper(root.right);
    
    let root2any = Math.max(left.root2any, right.root2any, 0) + root.val;
    // compare with 0 because we can give up one path, if the path < 0

    let any2any = Math.max(left.any2any, right.any2any);
    // here we don't compare with 0, cannot give up the path, maybe two of them are all < 0
    
    any2any = Math.max(any2any, Math.max(0, left.root2any) + root.val + Math.max(right.root2any, 0)); 
    // check if < 0 for each .root2any
    
    return {root2any: root2any,
            any2any: any2any
    };
}

// consider the relationship between the results for root, root.left, root.right, max or addAll
// using .root2any .any2any data structure
// var maxPathSum = function(root) {
//     var result = helper(root);
//     return result.any2any;
// };

// function helper (root) {
//     // root to any node
//     // -Infinity capital I!!!!!
//     if (root === null) {
//         return {root2any: -Infinity, 
//                 any2any: -Infinity};
//     }
    
//     // call the helper itself, not the whole function
//     var left = helper(root.left);
//     var right = helper(root.right);
    
//     // don't forget it is .root2any!!! not left itself
//     var root2any = Math.max(0, left.root2any, right.root2any) + root.val;
//     var any2any = Math.max(left.any2any, right.any2any);

//     // any2any here is the max of all left, all right, left + root + right!!!!
//     any2any = Math.max(Math.max(0, left.root2any) + Math.max(0, right.root2any) + root.val, any2any);

//     return {root2any: root2any, 
//             any2any: any2any};
// }
