// 113. Path Sum II

// 113. Path Sum II   Add to List QuestionEditorial Solution  My Submissions
// Total Accepted: 111834
// Total Submissions: 352896
// Difficulty: Medium
// Contributors: Admin
// Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.

// For example:
// Given the below binary tree and sum = 22,
//               5
//              / \
//             4   8
//            /   / \
//           11  13  4
//          /  \    / \
//         7    2  5   1
// return
// [
//    [5,4,11,2],
//    [5,8,4,5]
// ]
// Hide Company Tags Bloomberg
// Hide Tags Tree Depth-first Search
// Hide Similar Problems (E) Path Sum (E) Binary Tree Paths (E) Path Sum III


//PM commits:
// MUST READ: https://www.geeksforgeeks.org/print-k-sum-paths-binary-tree/



/*
// C++ program to print all paths with sum k.

Input : k = 5
        Root of below binary tree:
           1
        /     \
      3        -1
    /   \     /   \
   2     1   4     5
        /   / \     \
       1   1   2     6

Output :
3 2
3 1 1
1 3 1
4 1
1 -1 4 1
-1 4 2
5
1 -1 5

#include <bits/stdc++.h>
using namespace std;

//utility function to print contents of
//a vector from index i to it's end
void printVector(const vector<int>& v, int i)
{
    for (int j=i; j<v.size(); j++)
    cout << v[j] << " ";
    cout << endl;
}

// binary tree node
struct Node
{
    int data;
    Node *left,*right;
    Node(int x)
    {
        data = x;
        left = right = NULL;
    }
};

// This function prints all paths that have sum k
void printKPathUtil(Node *root, vector<int>& path, int k)
{
    // empty node
    if (!root)
        return;

    // add current node to the path
    path.push_back(root->data);

    // check if there's any k sum path
    // in the left sub-tree.
    printKPathUtil(root->left, path, k);

    // check if there's any k sum path
    // in the right sub-tree.
    printKPathUtil(root->right, path, k);

    // check if there's any k sum path that
    // terminates at this node
    // Traverse the entire path as
    // there can be negative elements too
    int f = 0;
    for (int j=path.size()-1; j>=0; j--)
    {
        f += path[j];

        // If path sum is k, print the path
        if (f == k)
            printVector(path, j);
    }

    // Remove the current element from the path
    path.pop_back();
}

// A wrapper over printKPathUtil()
void printKPath(Node *root, int k)
{
    vector<int> path;
    printKPathUtil(root, path, k);
}

// Driver code
int main()
{
    Node *root = new Node(1);
    root->left = new Node(3);
    root->left->left = new Node(2);
    root->left->right = new Node(1);
    root->left->right->left = new Node(1);
    root->right = new Node(-1);
    root->right->left = new Node(4);
    root->right->left->left = new Node(1);
    root->right->left->right = new Node(2);
    root->right->right = new Node(5);
    root->right->right->right = new Node(2);

    int k = 5;
    printKPath(root, k);

    return 0;
}

*/

/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {number[][]}
 */
var pathSum = function(root, sum) {
    var results = [];
    if (root === null) {
        return results;
    }
    // path is the current nodes so far
    var path = [];
    path.push(root.val);
    helper(root, path, root.val, results, sum);
    return results;
};

function helper(root, path, subSum, results, sum) {
    // leaf
    if (root.left === null && root.right === null) {
        if (subSum === sum) {
            // deep copy
            results.push(path.slice()); 
        }
        return;
    }
    
    // go to left
    if (root.left !== null) {
        path.push(root.left.val);
        helper(root.left, path, subSum + root.left.val, results, sum);
        path.pop();
    }
    
    // go to right
    if (root.right !== null) {
        path.push(root.right.val);
        helper(root.right, path, subSum + root.right.val, results, sum);
        path.pop();
    }
}

